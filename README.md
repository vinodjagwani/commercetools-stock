# commercetools GmbH Stock service

The purpose of stock service is to
1. create stock for product
2. get stock for particular product
3. get top selling product and top available product

#Get Started

Project is based on maven build tool and docker and there are 2 sub maven modules
1. stock-service - actuall service needs to be build
2. stock-client  - feingclient which can be used to other services
if you don't have docker installed locally then run following maven command

    mvm clean install
    mvn spring-boot:run

note: To run locally mysql instance also needs to be run locally.

if you have docker installed locally then following command requires to run

    docker-compose up

the above command will automatically pull the service image as well mysql image.


or you can also build latest docker image from source code by using following maven command

    mvn clean package docker:build

This build the image of service locally and then docker-compose up can be use to run locally.


#Testing Strategy
Unit test and Integration test are in src/test/ folder you can run by using maven command

    mvn test


#Publishing strategy
Service can be published on docker hub registry public or private by using command

    mvn docker:push

# Request and Response representation
JSON documentation for the rest service can be found on:

- Run locally
    - [swagger-ui](http://localhost:9090/swagger-ui.html)


However actual service urls are secure with oauth2, so before accessing any of following api
we have to get access token by using following command:

curl -X POST --user 'web:secret' -d 'grant_type=client_credentials' http://localhost:9090/oauth/token

after getting access token, api can be accessed

1. http://localhost:9090/v1.0/stocks/updateStock?access_token=10145a32-105b-47e7-bf53-d24d01b9ece5

2. http://localhost:9090/v1.0/stocks?productId=product-2&access_token=9b1bf791-d798-459c-9c27-8f9fa89155d4

3. http://localhost:9090/v1.0/stocks/statistics?time=[today,lastMonth]&access_token=10145a32-105b-47e7-bf53-d24d01b9ece5

Above APIs will perform the functionality according to the given requirement document.


#Dependencies
The order projects depend on

* Java 8
* Maven 3.3 or higher
* Docker 1.8 or higher
* MySQL

Note: Stock service has following capabilities.

1. Security implemented by using spring oauth2.
2. Ready to use with central spring-cloud config server.
3. FeingCleint is implemented to communicate with other microservices without calling urls
   (in our case stock-client jar enough to use in any service)
4. Spring sleuth tracing ids are included for using with spring zipkin server for tracing requests
5. Rest Assured for creation of request response stub files
6. Docker maven plugin for building image and pushing image at docker hub
7. QueryDSL for spring jpa dynamic query generation using predicate
8. Swagger2 for rest documentation


- How you handle concurrent request against one endpoint?
   Since our service is highly scalable we can run multiple instance of service using
   container technology or on cloud and also spring cloud/netflix implementation support multiple
   feature for instance, internal load balancing, fail over and circuit breaker mechanism using
   round robing policy for our service, that can communicate very fast in case if there
   is no problem with network, regarding concurrent request it depend on the requirements
   how efficiently resources can be utilized and allocating threads only when the response
   is ready for processing, for instance event-driven approach work well to solve this kind of problem
   using asynchronous messaging queue technology and Future callbacks and executor service in java
   which create pool of threads but it can be complex to handle such things in distributed system

- Is the backend always in a valid state? - What about Race conditions?
  Since rest full service are stateless by in nature, so there is no best way to not rely
  on backend, however in distributed architecture we have also fallback mechanism and Retry Pattern which
  can help to avoid the race condition, and other important aspect what kind of data
  we have for instance i used mysql db using spring transactions, it will help me to prevent
  the race condition and also data will be consistent.

- Did you thought about some edge cases?
  When we are working on highly scalable distributed system there is a higher chance for network,
  level issues As a consequence of service dependencies, any component can be temporarily unavailable
  for their consumers or can be impact on the performance of application so to minimize the impact
  we need to build fault tolerant services that can gracefully respond to certain types of issues.

