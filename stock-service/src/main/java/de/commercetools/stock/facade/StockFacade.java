package de.commercetools.stock.facade;

import de.commercetools.stock.dto.ProductData;
import de.commercetools.stock.dto.StockData;
import de.commercetools.stock.dto.StockStatisticsData;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public interface StockFacade extends GenericFacade<StockData, Long> {

    ProductData findStockByProduct(final String productId);

    StockStatisticsData findStockStatistics(final String time);

}
