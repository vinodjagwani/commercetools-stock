package de.commercetools.stock.facade;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public interface GenericFacade<D, I> {

    D findById(I id);

    Page<D> findAll(final Predicate predicate, final Pageable pageable);

    void save(final D dto);

}
