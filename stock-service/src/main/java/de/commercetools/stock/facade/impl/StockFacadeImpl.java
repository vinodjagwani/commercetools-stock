package de.commercetools.stock.facade.impl;

import com.querydsl.core.types.Predicate;
import de.commercetools.stock.StockConstant;
import de.commercetools.stock.dto.ProductData;
import de.commercetools.stock.dto.StockData;
import de.commercetools.stock.dto.StockStatisticsData;
import de.commercetools.stock.dto.TopSellingData;
import de.commercetools.stock.entity.Product;
import de.commercetools.stock.entity.Stock;
import de.commercetools.stock.exception.DefinedErrorException;
import de.commercetools.stock.exception.ErrorEnum;
import de.commercetools.stock.facade.StockFacade;
import de.commercetools.stock.service.ProductService;
import de.commercetools.stock.service.StockService;
import de.commercetools.stock.service.StockStatisticsService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static de.commercetools.stock.StockConstant.STK_E_TOP_SEL_D_ID;
import static de.commercetools.stock.StockConstant.STOCK_E_D_ID;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Service
public class StockFacadeImpl implements StockFacade {


    @Autowired
    private Mapper dozerMaper;

    @Autowired
    private StockService stockService;

    @Autowired
    private ProductService productService;

    @Autowired
    private StockStatisticsService stockStatisticsService;


    @Override
    public StockData findById(final Long id) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<StockData> findAll(final Predicate predicate, final Pageable pageable) {
        return convert(stockService.findAll(predicate, pageable));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(final StockData dto) {
        final Product product = productService.findByCode(dto.getProductId());
        Stock stock = stockService.findStockByProduct(dto.getProductId());
        final Date timestamp = convertTimestampUTC(dto.getTimestamp());
        if (null != stock) {
            if (timestamp.before(stock.getTimestamp())) {
                throw new DefinedErrorException(ErrorEnum.OUT_DATED_STOCK, "Outdated stock");
            } else {
                stock.setQuantity(dto.getQuantity());
                stock.setTimestamp(timestamp);
            }
        } else {
            stock = dozerMaper.map(dto, Stock.class, StockConstant.STOCK_D_E_ID);
            stock.setProduct(product);
        }
        stockService.save(stock);
    }

    @Override
    @Transactional(readOnly = true)
    public ProductData findStockByProduct(final String productId) {
        final Stock stock = stockService.findStockByProduct(productId);
        if (null == stock) {
            throw new DefinedErrorException(ErrorEnum.ENTITY_NOT_FOUND, "Stock doesn't exit with this product");
        }
        final ProductData productData = dozerMaper.map(stock, ProductData.class, StockConstant.PRODUCT_STOCK_E_D_ID);
        productData.setRequestTimestamp(currentDateTimeUTC());
        return productData;
    }

    @Override
    @Transactional(readOnly = true)
    public StockStatisticsData findStockStatistics(final String time) {
        validateRangeParam(time);
        Pageable pageable = new PageRequest(0, 3, Sort.Direction.DESC, "quantity");
        StockStatisticsData stockStatisticsData = new StockStatisticsData();
        stockStatisticsData.setRange(time);
        stockStatisticsData.setRequestTimestamp(currentDateTimeUTC());
        Page<Stock> topAvailableProducts = stockStatisticsService.findTopAvailableProducts(pageable);
        Page<Stock> topSellingProducts = stockStatisticsService.findTopSellingProducts(pageable);
        setTopAvailableProducts(stockStatisticsData, topAvailableProducts);
        setTopSellingProducts(stockStatisticsData, topSellingProducts);
        return stockStatisticsData;
    }


    private void setTopAvailableProducts(final StockStatisticsData data, final Page<Stock> stocks) {
        List<StockData> topAvailableProduct = new ArrayList<>();
        long totalElements = stocks.getTotalElements();
        if (totalElements > 0) {
            stocks.getContent().forEach(stock -> topAvailableProduct.add(dozerMaper.map(stock, StockData.class, STOCK_E_D_ID)));
        }
        data.setTopAvailableProducts(topAvailableProduct);

    }

    private void setTopSellingProducts(final StockStatisticsData data, final Page<Stock> stocks) {
        List<TopSellingData> topSellingDataList = new ArrayList<>();
        long totalElement = stocks.getTotalElements();
        if (totalElement > 0) {
            stocks.getContent().forEach(stock -> topSellingDataList.add(dozerMaper.map(stock, TopSellingData.class, STK_E_TOP_SEL_D_ID)));
        }
        data.setTopSellingProducts(topSellingDataList);
    }

    private Page<StockData> convert(final Page<Stock> stocks) {
        return stocks.map(stock -> dozerMaper.map(stock, StockData.class, StockConstant.STOCK_E_D_ID));
    }

    private void validateRangeParam(final String param) {
        if (!StockConstant.RANGE_PARAM.equals(param)) {
            throw new DefinedErrorException(ErrorEnum.REQUEST_VALIDATION, "Invalid value for time");
        }
    }

    private Date convertTimestampUTC(final String timestamp) {
        final DateTimeFormatter format = DateTimeFormatter.ISO_DATE_TIME;
        LocalDateTime datetime = LocalDateTime.parse(timestamp, format);
        Instant toInstant = datetime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(toInstant);
    }

    private String currentDateTimeUTC() {
        DateTimeFormatter fm = DateTimeFormatter.ofPattern(StockConstant.UTC_FORMAT);
        ZonedDateTime atZone = LocalDateTime.now().atZone(ZoneId.of("UTC"));
        return atZone.format(fm);
    }
}
