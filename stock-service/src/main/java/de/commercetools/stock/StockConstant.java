package de.commercetools.stock;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public final class StockConstant {

    public static final String MAPPING_FILE = "classpath*:mapping/stock-bean-mappings.xml";
    public static final String STOCK_D_E_ID = "stock-d-e-id";
    public static final String STOCK_E_D_ID = "stock-e-d-id";
    public static final String PRODUCT_STOCK_E_D_ID = "product-stock-e-d-id";
    public static final String RANGE_PARAM = "[today,lastMonth]";
    public static final String STK_E_TOP_SEL_D_ID = "stock-e-top-selling-d-id";
    public static final String UTC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXX";

    private StockConstant() {
        throw new IllegalStateException("StockConstant Constant class");
    }
}
