package de.commercetools.stock.service;

import de.commercetools.stock.entity.Product;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public interface ProductService extends GenericService<Product, Long> {

    Product findByCode(final String code);
}
