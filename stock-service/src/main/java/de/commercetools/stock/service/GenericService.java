package de.commercetools.stock.service;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public interface GenericService<E, I> {

    E findById(I id);

    Page<E> findAll(final Predicate predicate, final Pageable pageable);

    void save(final E entity);

}
