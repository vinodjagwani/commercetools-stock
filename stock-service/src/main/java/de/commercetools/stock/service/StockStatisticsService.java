package de.commercetools.stock.service;

import de.commercetools.stock.entity.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StockStatisticsService {

    Page<Stock> findTopAvailableProducts(final Pageable pageable);

    Page<Stock> findTopSellingProducts(final Pageable pageable);

}
