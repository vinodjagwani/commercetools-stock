package de.commercetools.stock.service.impl;

import com.querydsl.core.types.Predicate;
import de.commercetools.stock.entity.Product;
import de.commercetools.stock.exception.DefinedErrorException;
import de.commercetools.stock.exception.ErrorEnum;
import de.commercetools.stock.repository.ProductRepository;
import de.commercetools.stock.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Service
public class ProductServiceImpl implements ProductService {


    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product findByCode(final String code) {
        final Product product = productRepository.findByCode(code);
        if (null == product) {
            throw new DefinedErrorException(ErrorEnum.ENTITY_NOT_FOUND, "Product doesn't exist");
        }
        return product;
    }

    @Override
    public Product findById(final Long id) {
        final Product product = productRepository.findOne(id);
        if (null == product) {
            throw new DefinedErrorException(ErrorEnum.ENTITY_NOT_FOUND, "Product doesn't exist");
        }
        return product;
    }

    @Override
    public Page<Product> findAll(final Predicate predicate, final Pageable pageable) {
        return productRepository.findAll(predicate, pageable);
    }

    @Override
    public void save(final Product entity) {
        productRepository.save(entity);
    }


}
