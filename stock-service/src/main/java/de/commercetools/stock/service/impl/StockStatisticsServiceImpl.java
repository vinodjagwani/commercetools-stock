package de.commercetools.stock.service.impl;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.DateTimePath;
import de.commercetools.stock.entity.QStock;
import de.commercetools.stock.entity.Stock;
import de.commercetools.stock.repository.StockRepository;
import de.commercetools.stock.service.StockStatisticsService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class StockStatisticsServiceImpl implements StockStatisticsService {


    @Autowired
    private StockRepository stockRepository;


    @Override
    public Page<Stock> findTopAvailableProducts(final Pageable pageable) {
        Predicate allOf = QStock.stock.product.available.eq(true).and(getRangePredicate());
        return stockRepository.findAll(allOf, pageable);
    }

    @Override
    public Page<Stock> findTopSellingProducts(final Pageable pageable) {
        final Predicate predicate = getRangePredicate();
        return stockRepository.findAll(predicate, pageable);
    }

    private Predicate getRangePredicate() {
        DateTime dateTime = new DateTime();
        Date now = dateTime.toDate();
        Date midnight = dateTime.withTimeAtStartOfDay().toDate();
        DateTime firstDate = dateTime.minusMonths(1).withDayOfMonth(1);
        DateTime lastDate = dateTime.withDayOfMonth(1).minusDays(1);
        DateTimePath<Date> timestamp = QStock.stock.timestamp;
        return timestamp.eq(midnight)
                .or(timestamp.gt(midnight))
                .and(timestamp.eq(now))
                .or(timestamp.lt(now))
                .and(timestamp.after(firstDate.toDate()))
                .and(timestamp.before(lastDate.toDate()));
    }
}
