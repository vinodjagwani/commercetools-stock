package de.commercetools.stock.service;

import de.commercetools.stock.entity.Stock;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public interface StockService extends GenericService<Stock, Long> {

    Stock findStockByProduct(final String code);
}
