package de.commercetools.stock.service.impl;

import com.querydsl.core.types.Predicate;
import de.commercetools.stock.entity.Stock;
import de.commercetools.stock.repository.StockRepository;
import de.commercetools.stock.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Service
public class StockServiceImpl implements StockService {


    @Autowired
    private StockRepository stockRepository;


    @Override
    public Stock findById(final Long id) {
        return null;
    }


    @Override
    public Stock findStockByProduct(final String code) {
        return stockRepository.findByProduct(code);
    }

    @Override
    public Page<Stock> findAll(final Predicate predicate, final Pageable pageable) {
        return stockRepository.findAll(predicate, pageable);
    }

    @Override
    public void save(final Stock entity) {
        stockRepository.save(entity);
    }

}
