package de.commercetools.stock.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product", indexes = {@Index(name = "IDX_PRODUCT_CODE", columnList = "code")})
public class Product implements Serializable {

    private static final long serialVersionUID = 7526472295622776147L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false)
    private Long productId;

    @Column(nullable = false, unique = true)
    private String code;

    @Column
    private String description;

    @Column
    private boolean available = true;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;


}
