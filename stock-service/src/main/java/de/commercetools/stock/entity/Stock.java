package de.commercetools.stock.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "stock", indexes = {@Index(name = "IDX_STOCK_CODE", columnList = "code")})
public class Stock implements Serializable {

    private static final long serialVersionUID = 8526472295622776147L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stockId", unique = true, nullable = false)
    private Long stockId;

    @Column(nullable = false, unique = true)
    private String code;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    private Integer quantity;

    @OneToOne
    @JoinColumn(name = "product_id", unique = true)
    private Product product;

}
