package de.commercetools.stock.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Getter
@Setter
public class DefinedErrorException extends RuntimeException {

    private ErrorPrinter errorEnum;

    private String errorCode;

    private HttpStatus httpStatus;

    private String message;


    public DefinedErrorException(ErrorPrinter errorEnum, final String message) {
        super(message);
        this.errorEnum = errorEnum;
        errorCode = errorEnum.getErrorCode();
        httpStatus = errorEnum.getHttpStatus();
        this.message = message;
    }
}
