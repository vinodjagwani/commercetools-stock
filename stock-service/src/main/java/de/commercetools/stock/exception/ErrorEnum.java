package de.commercetools.stock.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by vinodjagwani on 9/27/17.
 */
public enum ErrorEnum implements ErrorPrinter {

    REQUEST_VALIDATION(HttpStatus.BAD_REQUEST, "004"),
    OUT_DATED_STOCK(HttpStatus.NO_CONTENT, "005"),
    ENTITY_NOT_FOUND(HttpStatus.BAD_REQUEST, "002");


    private HttpStatus httpStatus;

    private String errorCode;


    ErrorEnum(HttpStatus httpStatus, String errorCode) {
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }


    @Override
    public String getErrorCode() {
        return errorCode;
    }

}
