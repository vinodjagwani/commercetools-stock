package de.commercetools.stock.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.mysql.jdbc.MysqlDataTruncation;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import de.commercetools.stock.dto.ErrorData;
import de.commercetools.stock.dto.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinodjagwani on 9/27/17.
 */
@Slf4j
@RestControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DefaultExceptionHandler {


    @Value("${service.identifier:90}")
    private String serviceIdentifier;

    private String INTERNAL_SERVER_ERROR;

    private String DATA_VALIDATION_ERROR;

    private String REQUEST_VALIDATION_ERROR;


    @PostConstruct
    public void init() {
        INTERNAL_SERVER_ERROR = serviceIdentifier + "000";
        DATA_VALIDATION_ERROR = serviceIdentifier + "003";
        REQUEST_VALIDATION_ERROR = serviceIdentifier + "001";
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> invalidInputRequest(MethodArgumentNotValidException ex) {
        final ErrorResponse response = ErrorResponse.builder().build();
        final List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<ErrorData> errorDataList = new ArrayList<>();
        for (FieldError fieldError : fieldErrors) {
            ErrorData errorData = new ErrorData();
            errorData.setCode(REQUEST_VALIDATION_ERROR);
            errorData.setMessage(fieldError.getDefaultMessage());
            errorDataList.add(errorData);
        }
        response.setErrors(errorDataList);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<ErrorResponse> handleValidationIllegalError(InvalidFormatException ex) {
        ErrorResponse errorResponse = ErrorResponse.builder().build();
        List<ErrorData> errorDataList = new ArrayList<>();
        ErrorData errorData = new ErrorData();
        errorData.setCode(DATA_VALIDATION_ERROR);
        errorData.setMessage(ex.getPathReference());
        errorDataList.add(errorData);
        errorResponse.setErrors(errorDataList);
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(DefinedErrorException.class)
    public ResponseEntity<ErrorResponse> handleCustomExceptions(DefinedErrorException ex) {
        HttpStatus httpStatus = ex.getHttpStatus() != null ? ex.getHttpStatus() : HttpStatus.INTERNAL_SERVER_ERROR;
        final String errorCode = serviceIdentifier + ex.getErrorCode();
        ErrorResponse errorResponse = ErrorResponse.builder().build();
        List<ErrorData> errorDataList = new ArrayList<>();
        ErrorData errorData = new ErrorData();
        errorData.setCode(errorCode);
        errorData.setMessage(ex.getMessage());
        errorDataList.add(errorData);
        errorResponse.setErrors(errorDataList);
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }


    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorResponse> argMisMatchExceptions(MethodArgumentTypeMismatchException ex) {
        ErrorResponse errorResponse = ErrorResponse.builder().build();
        List<ErrorData> errorDataList = new ArrayList<>();
        ErrorData errorData = new ErrorData();
        final String rootMsg = ExceptionUtils.getRootCauseMessage(ex);
        errorData.setCode(REQUEST_VALIDATION_ERROR);
        errorData.setMessage(rootMsg);
        errorDataList.add(errorData);
        errorResponse.setErrors(errorDataList);
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> unhandledExceptions(Exception ex) {
        ErrorResponse errorResponse = ErrorResponse.builder().build();
        List<ErrorData> errorDataList = new ArrayList<>();
        ErrorData errorData = new ErrorData();
        final String message = ExceptionUtils.getRootCauseMessage(ex);
        errorData.setCode(DATA_VALIDATION_ERROR);
        errorData.setMessage(message);
        errorDataList.add(errorData);
        errorResponse.setErrors(errorDataList);
        log.error(errorResponse.toString(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(ServletRequestBindingException.class)
    public ResponseEntity<ErrorResponse> controllerExceptions(ServletRequestBindingException ex) {
        ErrorResponse errorResponse = ErrorResponse.builder().build();
        List<ErrorData> errorDataList = new ArrayList<>();
        ErrorData errorData = new ErrorData();
        final String message = ExceptionUtils.getRootCauseMessage(ex);
        errorData.setCode(DATA_VALIDATION_ERROR);
        errorData.setMessage(message);
        errorDataList.add(errorData);
        errorResponse.setErrors(errorDataList);
        log.error("service returned error" + errorResponse);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> handleDuplicateRecordExceptions(DataIntegrityViolationException ex) {
        ErrorResponse errorResponse = ErrorResponse.builder().build();
        List<ErrorData> errorDataList = new ArrayList<>();
        ErrorData errorData = new ErrorData();
        String rootMsg = ExceptionUtils.getRootCauseMessage(ex);
        Throwable throwable = ex.getMostSpecificCause();
        if (throwable instanceof MySQLIntegrityConstraintViolationException) {
            String msg = throwable.getMessage();
            if (msg.contains("UK")) {
                msg = msg.substring(msg.indexOf("UK"), msg.length() - 1);

            }
            rootMsg = DuplicateKeyMapper.getField().get(msg);
        }
        if (throwable instanceof MysqlDataTruncation) {
            rootMsg = "Data is too long for one of the given column";
        }
        errorData.setCode(DATA_VALIDATION_ERROR);
        errorData.setMessage(rootMsg);
        errorDataList.add(errorData);
        errorResponse.setErrors(errorDataList);
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponse> handleValidationIllegalError(HttpMessageNotReadableException ex) {
        ErrorResponse errorResponse = ErrorResponse.builder().build();
        List<ErrorData> errorDataList = new ArrayList<>();
        ErrorData errorData = new ErrorData();
        errorData.setCode(REQUEST_VALIDATION_ERROR);
        Throwable throwable = ex.getRootCause();
        if (throwable instanceof InvalidFormatException) {
            JsonMappingException.Reference fieldName = ((InvalidFormatException) throwable).getPath().get(0);
            errorData.setMessage(fieldName.getFieldName() + " is invalid");
        } else {
            errorData.setMessage("Request body is invalid");
        }
        errorDataList.add(errorData);
        errorResponse.setErrors(errorDataList);
        log.error(ex.getMostSpecificCause().getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
}
