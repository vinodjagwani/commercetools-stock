package de.commercetools.stock.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by vinodjagwani on 9/27/17.
 */
public interface ErrorPrinter {

    HttpStatus getHttpStatus();

    String getErrorCode();

}
