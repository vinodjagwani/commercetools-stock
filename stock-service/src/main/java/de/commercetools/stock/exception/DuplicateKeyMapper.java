package de.commercetools.stock.exception;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DuplicateKeyMapper {


    public static Map<String, String> getField() {
        Map<String, String> map = new HashMap<>();
        map.put("UK_8pbyo6rdbktdur7u8vpc9r27n", "Stock already exist with given id");
        map.put("UK_khabtqwr86p7x9mt2krib98tx", "Stock already exist with given productId");
        map.put("UK_8PBYO6RDBKTDUR7U8VPC9R27N_INDEX_4", "Stock already exist with given id");
        map.put("UK_KHABTQWR86P7X9MT2KRIB98TX_INDEX_4", "Stock already exist with given productId");
        return Collections.unmodifiableMap(map);
    }

}
