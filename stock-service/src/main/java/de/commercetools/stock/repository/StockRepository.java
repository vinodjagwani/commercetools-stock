package de.commercetools.stock.repository;

import de.commercetools.stock.entity.Stock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public interface StockRepository extends PagingAndSortingRepository<Stock, Long>, QueryDslPredicateExecutor<Stock> {

    Stock findByCode(final String code);

    @Query(nativeQuery = true, value = "select s.* FROM stock s inner join product p on p.product_id=s.product_id where p.code=:code")
    Stock findByProduct(@Param("code") final String code);
}
