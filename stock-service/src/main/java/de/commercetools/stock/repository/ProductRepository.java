package de.commercetools.stock.repository;

import de.commercetools.stock.entity.Product;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by vinodjagwani on 4/12/17.
 */
public interface ProductRepository extends PagingAndSortingRepository<Product, Long>, QueryDslPredicateExecutor<Product> {

    Product findByCode(final String code);
}
