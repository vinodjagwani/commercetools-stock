package de.commercetools.stock.controller;

import de.commercetools.stock.dto.ProductData;
import de.commercetools.stock.dto.StockData;
import de.commercetools.stock.dto.StockStatisticsData;
import de.commercetools.stock.facade.StockFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Slf4j
@RestController
@Api(tags = "Stock")
@RequestMapping(path = "/v1.0/stocks")
public class StockController {


    @Autowired
    private StockFacade stockFacade;


    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(value = "Get available stock of product")
    public ResponseEntity<ProductData> getStockByProduct(@RequestParam("productId") String productId) {
        log.info("Get available stock of product with productId: {}", productId);
        return new ResponseEntity<>(stockFacade.findStockByProduct(productId), HttpStatus.OK);
    }

    @RequestMapping(
            path = "/statistics",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(value = "Get statistics of stock")
    public ResponseEntity<StockStatisticsData> getStockStatistics(@RequestParam("time") String time) {
        log.info("Get statistics of stock with given range: {}", time);
        return new ResponseEntity<>(stockFacade.findStockStatistics(time), HttpStatus.OK);
    }

    @RequestMapping(
            path = "/updateStock",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ApiOperation(value = "Update stock of particular product")
    public ResponseEntity<Void> updateStock(@Valid @RequestBody StockData stockData) {
        log.info("Update stock of particular product: {}", stockData);
        stockFacade.save(stockData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}
