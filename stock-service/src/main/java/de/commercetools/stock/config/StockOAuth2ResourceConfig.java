package de.commercetools.stock.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Configuration
@EnableResourceServer
public class StockOAuth2ResourceConfig extends ResourceServerConfigurerAdapter {


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/v1.0/**").authenticated();
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
}
