package de.commercetools.stock.config;

import de.commercetools.stock.StockConstant;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.TimeZone;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Configuration
public class StockConfig {


    @PostConstruct
    void setTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }


    @Bean(name = "dozerMapper")
    public DozerBeanMapperFactoryBean configDozer() throws IOException {
        DozerBeanMapperFactoryBean mapper = new DozerBeanMapperFactoryBean();
        PathMatchingResourcePatternResolver pathResolver = new PathMatchingResourcePatternResolver();
        final Resource[] resources = pathResolver.getResources(StockConstant.MAPPING_FILE);
        mapper.setMappingFiles(resources);
        return mapper;
    }
}
