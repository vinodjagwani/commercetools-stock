package de.commercetools.stock;

import de.commercetools.stock.dto.ProductData;
import de.commercetools.stock.dto.StockData;
import de.commercetools.stock.dto.StockStatisticsData;
import de.commercetools.stock.dto.TopSellingData;
import de.commercetools.stock.entity.Product;
import de.commercetools.stock.entity.Stock;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;

public abstract class AbstractStockTest {


    public Stock getStock() {
        Stock stock = new Stock();
        stock.setCode("stock-123345");
        stock.setQuantity(99);
        stock.setTimestamp(LocalDateTime.now().toDate(TimeZone.getTimeZone("UTC")));
        return stock;
    }

    public StockData getStockData() {
        final Stock stock = getStock();
        StockData stockData = new StockData();
        stockData.setId("stock-12");
        stockData.setProductId(getProduct().getCode());
        stockData.setTimestamp("2013-08-12T19:20:30.000Z");
        stockData.setQuantity(stock.getQuantity());
        return stockData;
    }

    public ProductData getProductData() {
        ProductData productData = new ProductData();
        productData.setRequestTimestamp("2013-08-12T19:20:30.000Z");
        productData.setProductId(getProduct().getCode());
        productData.setStock(getStockData());
        return productData;
    }

    public Product getProduct() {
        Product product = new Product();
        product.setAvailable(true);
        product.setCode("product-12345");
        product.setTimestamp(new Date());
        return product;
    }

    public StockStatisticsData getStockStatisticsData() {
        StockStatisticsData statisticsData = new StockStatisticsData();
        statisticsData.setRange("[today,lastMonth]");
        statisticsData.setRequestTimestamp(new DateTime().toString());
        statisticsData.setTopAvailableProducts(Collections.singletonList(getStockData()));
        statisticsData.setTopSellingProducts(Collections.singletonList(getTopSellingData()));
        return statisticsData;
    }

    public TopSellingData getTopSellingData() {
        TopSellingData topSellingData = new TopSellingData();
        topSellingData.setProductId("vegetables-1234");
        topSellingData.setQuantity(99);
        return topSellingData;
    }
}
