package de.commercetools.stock.service;


import com.querydsl.core.types.Predicate;
import de.commercetools.stock.AbstractStockTest;
import de.commercetools.stock.entity.Stock;
import de.commercetools.stock.repository.StockRepository;
import de.commercetools.stock.service.impl.StockStatisticsServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StockStatisticsServiceTest extends AbstractStockTest {


    @Mock
    private StockRepository stockRepository;

    @InjectMocks
    private StockStatisticsServiceImpl stockStatisticsService;


    @BeforeClass
    public static void setUp() {
        MockitoAnnotations.initMocks(StockStatisticsServiceImpl.class);
    }


    @Test
    public void testFindTopAvailableProducts() throws Exception {
        Pageable pageable = new PageRequest(0, 3, Sort.Direction.DESC, "quantity");
        final List<Stock> stockList = Collections.singletonList(getStock());
        Page<Stock> stockPage = new PageImpl<>(stockList);
        when(stockRepository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(stockPage);
        stockStatisticsService.findTopAvailableProducts(pageable);
        verify(stockRepository, times(1)).findAll(any(Predicate.class), any(Pageable.class));
    }


    @Test
    public void testFindTopSellingProducts() throws Exception {
        Pageable pageable = new PageRequest(0, 3, Sort.Direction.DESC, "quantity");
        final List<Stock> stockList = Collections.singletonList(getStock());
        Page<Stock> stockPage = new PageImpl<>(stockList);
        when(stockRepository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(stockPage);
        stockStatisticsService.findTopSellingProducts(pageable);
        verify(stockRepository, times(1)).findAll(any(Predicate.class), any(Pageable.class));
    }
}
