package de.commercetools.stock.service;


import com.querydsl.core.types.Predicate;
import de.commercetools.stock.AbstractStockTest;
import de.commercetools.stock.entity.Product;
import de.commercetools.stock.repository.ProductRepository;
import de.commercetools.stock.service.impl.ProductServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest extends AbstractStockTest {


    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;


    @BeforeClass
    public static void setUp() {
        MockitoAnnotations.initMocks(ProductServiceImpl.class);
    }


    @Test
    public void testFindAll() throws Exception {
        final List<Product> productList = Collections.singletonList(getProduct());
        Page<Product> productPage = new PageImpl<>(productList);
        when(productRepository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(productPage);
        productService.findAll(any(Predicate.class), any(Pageable.class));
        verify(productRepository, times(1)).findAll(any(Predicate.class), any(Pageable.class));
    }

    @Test
    public void testSave() {
        final Product product = getProduct();
        when(productRepository.save(any(Product.class))).thenReturn(product);
        productService.save(product);
        verify(productRepository, times(1)).save(product);
    }
}
