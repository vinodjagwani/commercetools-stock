package de.commercetools.stock.service;

import com.querydsl.core.types.Predicate;
import de.commercetools.stock.AbstractStockTest;
import de.commercetools.stock.entity.Stock;
import de.commercetools.stock.repository.StockRepository;
import de.commercetools.stock.service.impl.StockServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class StockServiceTest extends AbstractStockTest {


    @Mock
    private StockRepository stockRepository;

    @InjectMocks
    private StockServiceImpl stockService;


    @BeforeClass
    public static void setUp() {
        MockitoAnnotations.initMocks(StockServiceImpl.class);
    }


    @Test
    public void testFindStockByProduct() throws Exception {
        final Stock stock = getStock();
        when(stockRepository.findByProduct(any(String.class))).thenReturn(stock);
        stockService.findStockByProduct("product-123");
        verify(stockRepository, times(1)).findByProduct("product-123");
    }


    @Test
    public void testFindAll() throws Exception {
        final List<Stock> stockList = Collections.singletonList(getStock());
        Page<Stock> stockPage = new PageImpl<>(stockList);
        when(stockRepository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(stockPage);
        stockService.findAll(any(Predicate.class), any(Pageable.class));
        verify(stockRepository, times(1)).findAll(any(Predicate.class), any(Pageable.class));
    }

    @Test
    public void testSave() {
        final Stock stock = getStock();
        when(stockRepository.save(any(Stock.class))).thenReturn(stock);
        stockService.save(stock);
        verify(stockRepository, times(1)).save(stock);
    }

}
