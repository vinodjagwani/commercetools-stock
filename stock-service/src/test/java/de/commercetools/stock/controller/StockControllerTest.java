package de.commercetools.stock.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import de.commercetools.stock.AbstractStockTest;
import de.commercetools.stock.dto.ProductData;
import de.commercetools.stock.dto.StockData;
import de.commercetools.stock.dto.StockStatisticsData;
import de.commercetools.stock.facade.StockFacade;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.wiremock.restdocs.WireMockRestDocs;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@RunWith(SpringRunner.class)
@EnableSpringDataWebSupport
@AutoConfigureRestDocs(outputDir = "target/generated-docs")
@WebMvcTest(value = StockController.class, secure = false)
public class StockControllerTest extends AbstractStockTest {


    private static final ObjectMapper mapper = new ObjectMapper();


    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private StockFacade stockFacade;


    @Test
    public void getStockByProduct() throws Exception {
        final ProductData productData = getProductData();
        doReturn(productData).when(stockFacade).findStockByProduct(any(String.class));
        mockMvc.perform(get("/v1.0/stocks").param("productId", "vegetable-123"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.productId", Matchers.equalTo("product-12345")))
                .andDo(document("stock-by-product"))
                .andDo(WireMockRestDocs.verify()
                        .wiremock(WireMock.get(WireMock.urlMatching("/v1.0/stocks")))
                        .stub("stock-by-product"));
    }

    @Test
    public void testGetStockStatistics() throws Exception {
        final StockStatisticsData statisticsData = getStockStatisticsData();
        doReturn(statisticsData).when(stockFacade).findStockStatistics(any(String.class));
        mockMvc.perform(get("/v1.0/stocks/statistics")
                .param("time", "[today,lastMonth]"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.topAvailableProducts[0].id", Matchers.equalTo("stock-12")))
                .andDo(document("stock-statistics"))
                .andDo(WireMockRestDocs.verify()
                        .wiremock(WireMock.get(WireMock.urlMatching("/v1.0/stocks/statistics")))
                        .stub("stock-statistics"));
    }

    @Test
    public void testGetStockStatisticsWithInValidParam() throws Exception {
        final StockStatisticsData statisticsData = getStockStatisticsData();
        doReturn(statisticsData).when(stockFacade).findStockStatistics(any(String.class));
        mockMvc.perform(get("/v1.0/stocks/statistics")
                .param("time1", "[today,lastMonth]"))
                .andDo(print()).andExpect(status().is5xxServerError())
                .andDo(document("stock-statistics-invalid"))
                .andDo(WireMockRestDocs.verify()
                        .wiremock(WireMock.get(WireMock.urlMatching("/v1.0/stocks/statistics")))
                        .stub("stock-statistics-invalid"));

    }

    @Test
    public void testUpdateStock() throws Exception {
        final StockData stockData = getStockData();
        final String valueAsString = mapper.writeValueAsString(stockData);
        doNothing().when(stockFacade).save(any(StockData.class));
        mockMvc.perform(post("/v1.0/stocks/updateStock")
                .content(valueAsString)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print()).andExpect(status().isCreated())
                .andDo(document("update-stock"))
                .andDo(WireMockRestDocs.verify()
                        .wiremock(WireMock.post(WireMock.urlMatching("/v1.0/stocks/updateStock"))
                                .withRequestBody(WireMock.matchingJsonPath("$.id")))
                        .stub("update-stock"));

    }
}
