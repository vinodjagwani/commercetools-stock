package de.commercetools.stock.facade;

import com.querydsl.core.types.Predicate;
import de.commercetools.stock.AbstractStockTest;
import de.commercetools.stock.StockConstant;
import de.commercetools.stock.dto.ProductData;
import de.commercetools.stock.dto.StockData;
import de.commercetools.stock.entity.Product;
import de.commercetools.stock.entity.Stock;
import de.commercetools.stock.exception.DefinedErrorException;
import de.commercetools.stock.facade.impl.StockFacadeImpl;
import de.commercetools.stock.service.ProductService;
import de.commercetools.stock.service.StockService;
import de.commercetools.stock.service.StockStatisticsService;
import org.dozer.Mapper;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class StockFacadeTest extends AbstractStockTest {


    @Mock
    private Mapper dozerMaper;

    @Mock
    private StockService stockService;

    @Mock
    private ProductService productService;

    @InjectMocks
    private StockFacadeImpl stockFacade;

    @Mock
    private StockStatisticsService stockStatisticsService;


    @BeforeClass
    public static void setUp() {
        MockitoAnnotations.initMocks(StockFacadeImpl.class);
    }


    @Test
    public void testFindStockByProduct() throws Exception {
        final Stock stock = getStock();
        final ProductData productData = getProductData();
        when(stockService.findStockByProduct(any(String.class))).thenReturn(stock);
        when(dozerMaper.map(stock, ProductData.class, StockConstant.PRODUCT_STOCK_E_D_ID)).thenReturn(productData);
        stockFacade.findStockByProduct("product-123");
        verify(stockService, times(1)).findStockByProduct("product-123");
    }

    @Test(expected = DefinedErrorException.class)
    public void testFindStockByInvalidProduct() throws Exception {
        when(stockService.findStockByProduct(any(String.class))).thenReturn(null);
        stockFacade.findStockByProduct("product-123");
        verify(stockService, times(1)).findStockByProduct("product-123");
    }

    @Test
    public void testFindAll() throws Exception {
        final List<Stock> stockList = Collections.singletonList(getStock());
        Page<Stock> stockPage = new PageImpl<>(stockList);
        when(stockService.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(stockPage);
        stockFacade.findAll(any(Predicate.class), any(Pageable.class));
        verify(stockService, times(1)).findAll(any(Predicate.class), any(Pageable.class));
    }

    @Test
    public void testSave() throws Exception {
        final StockData stockData = getStockData();
        final Product product = getProduct();
        final Stock stock = getStock();
        when(productService.findByCode(any(String.class))).thenReturn(product);
        when(dozerMaper.map(stockData, Stock.class, StockConstant.STOCK_D_E_ID)).thenReturn(stock);
        doNothing().when(stockService).save(any(Stock.class));
        stockFacade.save(stockData);
        verify(stockService, times(1)).save(stock);
    }

    @Test(expected = DefinedErrorException.class)
    public void testSave_outdatedStock() throws Exception {
        final StockData stockData = getStockData();
        final Product product = getProduct();
        final Stock stock = getStock();
        when(productService.findByCode(any(String.class))).thenReturn(product);
        when(stockService.findStockByProduct(any(String.class))).thenReturn(stock);
        when(dozerMaper.map(stockData, Stock.class, StockConstant.STOCK_D_E_ID)).thenReturn(stock);
        doNothing().when(stockService).save(any(Stock.class));
        stockFacade.save(stockData);
        verify(stockService, times(1)).save(stock);
    }


    @Test
    public void testFindStockStatistics() throws Exception {
        Pageable pageable = new PageRequest(0, 3, Sort.Direction.DESC, "quantity");
        final List<Stock> stockList = Collections.singletonList(getStock());
        Page<Stock> stockPage = new PageImpl<>(stockList, pageable, 3);
        when(stockStatisticsService.findTopAvailableProducts(any(Pageable.class))).thenReturn(stockPage);
        when(stockStatisticsService.findTopSellingProducts(any(Pageable.class))).thenReturn(stockPage);
        stockFacade.findStockStatistics("[today,lastMonth]");
        verify(stockStatisticsService, times(1)).findTopAvailableProducts(pageable);
        verify(stockStatisticsService, times(1)).findTopSellingProducts(pageable);
    }
}
