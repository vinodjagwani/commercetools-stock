package de.commercetools.stock.repository;


import de.commercetools.stock.AbstractStockTest;
import de.commercetools.stock.entity.Product;
import org.apache.commons.collections4.IteratorUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Iterator;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;

@DataJpaTest
@RunWith(SpringRunner.class)
public class ProductRepositoryTest extends AbstractStockTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testFindOne() {
        final Product product = productRepository.findOne(1L);
        Assert.assertNotNull(product);
    }

    @Test
    public void testFindByCode() {
        final Product product = productRepository.findByCode("vegetable-1");
        Assert.assertNotNull(product);
    }

    @Test
    public void testFindAll() {
        final Iterator<Product> iterator = productRepository.findAll().iterator();
        final List<Product> productList = IteratorUtils.toList(iterator);
        Assert.assertThat(productList, hasSize(15));
    }

    @Test
    public void testSave() {
        Product product = getProduct();
        product = productRepository.save(product);
        Assert.assertEquals(product.getCode(), "product-12345");
    }

    @Test
    public void testUpdate() {
        final Product product = productRepository.findOne(1L);
        Assert.assertEquals(product.getCode(), "vegetable-1");
        product.setCode("product-222");
        productRepository.save(product);
        Assert.assertEquals(product.getCode(), "product-222");
    }

}
