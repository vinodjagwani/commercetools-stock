package de.commercetools.stock.repository;


import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.DateTimePath;
import de.commercetools.stock.AbstractStockTest;
import de.commercetools.stock.entity.QStock;
import de.commercetools.stock.entity.Stock;
import org.apache.commons.collections4.IteratorUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@DataJpaTest
@RunWith(SpringRunner.class)
public class StockRepositoryTest extends AbstractStockTest {


    @Autowired
    private StockRepository stockRepository;

    @Test
    public void testFindOne() {
        final Stock stock = stockRepository.findOne(1L);
        Assert.assertNotNull(stock);
    }

    @Test
    public void testFindAll() {
        final Iterator<Stock> iterator = stockRepository.findAll().iterator();
        final List<Stock> stockList = IteratorUtils.toList(iterator);
        Assert.assertThat(stockList, hasSize(14));
    }

    @Test
    public void testSave() {
        Stock stock = getStock();
        stock = stockRepository.save(stock);
        Assert.assertEquals(stock.getQuantity(), Integer.valueOf(99));
    }

    @Test
    public void testUpdate() {
        final Stock stock = stockRepository.findOne(1L);
        Assert.assertEquals(stock.getCode(), "stock-123");
        stock.setCode("stock-222");
        stockRepository.save(stock);
        Assert.assertEquals(stock.getCode(), "stock-222");
    }

    @Test
    public void testTopAvailableProducts() {
        Predicate rangePredicate = getRangePredicate();
        Predicate productAvailable = QStock.stock.product.available.eq(true);
        Predicate allOf = ExpressionUtils.allOf(productAvailable, rangePredicate);
        Page<Stock> stocks = stockRepository.findAll(allOf, getPageable());
        Assert.assertEquals(stocks.getTotalElements(), 3);
        Assert.assertThat(stocks.getContent(), hasItem(hasProperty("quantity", is(800))));
        Assert.assertThat(stocks.getContent(), hasItem(hasProperty("quantity", is(780))));
        Assert.assertThat(stocks.getContent(), hasItem(hasProperty("quantity", is(100))));
    }


    @Test
    public void testTopSellingProducts() {
        Predicate rangePredicate = getRangePredicate();
        Page<Stock> stocks = stockRepository.findAll(rangePredicate, getPageable());
        Assert.assertEquals(stocks.getTotalPages(), 2);
        Assert.assertEquals(stocks.getTotalElements(), 6);
        Assert.assertThat(stocks.getContent(), hasItem(hasProperty("quantity", is(800))));
        Assert.assertThat(stocks.getContent(), hasItem(hasProperty("quantity", is(780))));
        Assert.assertThat(stocks.getContent(), hasItem(hasProperty("quantity", is(654))));

    }


    private Predicate getRangePredicate() {
        DateTime dateTime = new DateTime();
        Date now = dateTime.toDate();
        Date midnight = dateTime.withTimeAtStartOfDay().toDate();
        DateTime firstDate = dateTime.minusMonths(1).withDayOfMonth(1);
        DateTime lastDate = dateTime.withDayOfMonth(1).minusDays(1);
        DateTimePath<Date> timestamp = QStock.stock.timestamp;
        return timestamp.eq(midnight)
                .or(timestamp.gt(midnight))
                .and(timestamp.eq(now))
                .or(timestamp.lt(now))
                .and(timestamp.after(firstDate.toDate()))
                .and(timestamp.before(lastDate.toDate()));
    }


    private Pageable getPageable() {
        return new PageRequest(0, 3, new Sort(Sort.Direction.DESC, "quantity"));
    }
}
