package de.commercetools.stock.validation;

import de.commercetools.stock.validation.annotation.DateTimeFormat;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Slf4j
public class DateTimeValidator implements ConstraintValidator<DateTimeFormat, String> {


    @Override
    public void initialize(DateTimeFormat dateTimeFormat) {
        String pattern = dateTimeFormat.pattern();
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext context) {
        final DateTimeFormatter format = DateTimeFormatter.ISO_DATE_TIME;
        if (null == object) {
            return true;
        }
        try {
            LocalDateTime datetime = LocalDateTime.parse(object, format);
            if (null != datetime) {
                return true;
            }
        } catch (Exception ex) {
            log.error("Date format is incorrect", ex);
            return false;
        }
        return false;
    }
}
