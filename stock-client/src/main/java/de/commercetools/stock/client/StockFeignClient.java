package de.commercetools.stock.client;


import de.commercetools.stock.dto.StockData;
import de.commercetools.stock.dto.StockStatisticsData;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@FeignClient(url = "${stock.service.url}", value = "stock-service")
public interface StockFeignClient {


    @RequestMapping(
            path = "/v1.0/stocks",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    ResponseEntity<StockData> getStockByProduct(@RequestParam("productId") String productId);

    @RequestMapping(
            path = "/v1.0/stocks/statistics",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    ResponseEntity<StockStatisticsData> getStockStatistics(@RequestParam("time") String time);

    @RequestMapping(
            path = "/v1.0/stocks/updateStock",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    ResponseEntity<Void> updateStock(@Valid @RequestBody StockData stockData);

}
