package de.commercetools.stock.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Data
@JsonAutoDetect
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class StockStatisticsData {


    private String requestTimestamp;

    private String range;

    private List<StockData> topAvailableProducts;

    private List<TopSellingData> topSellingProducts;
}
