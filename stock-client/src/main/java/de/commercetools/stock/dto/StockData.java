package de.commercetools.stock.dto;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import de.commercetools.stock.validation.annotation.DateTimeFormat;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Data
@JsonAutoDetect
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class StockData {

    @NotBlank
    private String id;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private String timestamp;

    @NotBlank
    private String productId;

    @NotNull
    private Integer quantity;

}
