package de.commercetools.stock.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Data
@JsonAutoDetect
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class TopSellingData {


    private String productId;

    private Integer quantity;
}
