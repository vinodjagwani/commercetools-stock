package de.commercetools.stock.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Data
@JsonAutoDetect
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ProductData {

    @NotBlank
    private String productId;

    private String requestTimestamp;

    private StockData stock;

}
