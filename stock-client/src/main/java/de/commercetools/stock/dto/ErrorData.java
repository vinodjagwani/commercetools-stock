package de.commercetools.stock.dto;


import lombok.Data;

/**
 * Created by vinodjagwani on 4/12/17.
 */
@Data
public class ErrorData {

    private String code;

    private String message;

}


